$(document).ready(function() {

  $("#roles_table").DataTable({
    "paging": false,
    "info": false
  });
  
  $("#templates_table").DataTable({
    "paging": false,
    "info": false
  });



  $("#playbooks_table").DataTable({
    "paging": false,
    "info": false,
    "columnDefs": [
      {
        "visible": false,
        "targets": 0
      }
    ],
    "rowGroup": {
      dataSrc: 0
    }
  });

});
