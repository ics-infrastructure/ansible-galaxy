import datetime
import json
from pathlib import Path
from flask import Flask, g, render_template, jsonify


app = Flask(__name__)
app.config.from_mapping(
    FREEZER_DESTINATION="../public",
    FREEZER_BASE_URL="http://ics-infrastructure.pages.esss.lu.se/ansible-galaxy",
    FREEZER_DEFAULT_MIMETYPE="application/json",
    GALAXY_INFO=Path(app.root_path) / ".." / "galaxy.json",
    TEMPLATES_INFO=Path(app.root_path) / ".." / "tower_templates.json",
    WORKFLOW_TEMPLATES_INFO=Path(app.root_path) / ".." / "workflow_job_templates.json",
)


def galaxy_info():
    info = getattr(g, "_galaxy_info", None)
    if info is None:
        with open(app.config["GALAXY_INFO"]) as f:
            info = g._galaxy_info = json.load(f)
    return info


def templates_info():
    info = getattr(g, "_templates_info", None)
    if info is None:
        with open(app.config["TEMPLATES_INFO"]) as f:
            info = g._templates_info = json.load(f)
    return info


def workflow_templates_info():
    info = getattr(g, "_workflow_templates_info", None)
    if info is None:
        with open(app.config["WORKFLOW_TEMPLATES_INFO"]) as f:
            info = g._workflow_templates_info = json.load(f)
    return info


@app.context_processor
def current_time():
    return dict(current_time=datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M"))


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/playbooks/")
def list_playbooks():
    info = galaxy_info()
    roles_tag = {role["name"]: role["tag"] for role in info["roles"].values()}
    return render_template(
        "playbooks.html", playbooks=info["playbooks"].values(), roles_tag=roles_tag
    )


@app.route("/roles/")
def list_roles():
    info = galaxy_info()
    return render_template("roles.html", roles=info["roles"].values())


@app.route("/templates/")
def list_templates():
    info = templates_info()
    info_workflow = workflow_templates_info()
    return render_template(
        "templates.html",
        templates=info["results"],
        workflow_templates=info_workflow["results"],
    )


@app.route("/api/summary")
def summary():
    """Return all information about roles and playbooks as json"""
    info = galaxy_info()
    return jsonify(info)
