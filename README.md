# ansible-galaxy

This repository contains all ICS Ansible roles and playbooks from the
[ics-ansible-galaxy](https://gitlab.esss.lu.se/ics-ansible-galaxy) group.

Also included are all workflow templates and templates from AWX.

It uses Jinja2, Flask and Frozen-Flask for site creation.

It is automatically updated by a GitLab bot: [galaxy-bot](https://gitlab.esss.lu.se/ics-infrastructure/galaxy-bot).


License
-------

MIT
